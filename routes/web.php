<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/admin', function () {
    return view('pages.admin');
});

Route::middleware(['auth'])->group(function () {

  Route::resources([
      'agenda' => 'AgendaController',
      'lembretes' => 'LembretesController',
  ]);

});


// Route::get('/usuarios', function () {
//     return view('pages.usuarios');
// });

// Route::get('/media', 'FirstController@media');
//
// Route::get('/nomes', 'FirstController@nomes');
//
// Route::get('/soma', 'FirstController@soma');
//
// Route::get('/lista', 'FirstController@lista');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
