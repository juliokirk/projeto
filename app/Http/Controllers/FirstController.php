<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FirstController extends Controller
{
  public function media()
  {
    $data = [
      'num1' => 1,
      'num2' => 3,
      'num3' => 5,
      'num4' => 7,
      'num5' => 9,
    ];

    $media = ($data['num1']+$data['num2']+$data['num3']+$data['num4']+$data['num5']) / 5;

    return view('pages.media', compact('media'));
  }

  public function nomes()
  {
    $data = ['Bia', 'Luisa'];

    if ($data[0]===$data[1]) {

      echo "Os nomes <strong>$data[0]</strong> e <strong>$data[1]</strong> são iguais.";

      return view('pages.nomes');

    }
    else {

      echo "Os nomes <strong>$data[0]</strong> e <strong>$data[1]</strong> não são iguais.";

      return view('pages.nomes');

    }
  }

  public function soma()
  {
    $sum = 336 + 336;

    return view('pages.soma', compact('sum'));
  }

  public function lista()
  {

    $data = ['Thiago', 'Henrique', 'Pedro', 'João'];

    return view('pages.lista', compact('data'));

  }

}
