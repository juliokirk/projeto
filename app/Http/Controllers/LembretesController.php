<?php

namespace App\Http\Controllers;

use App\Lembrete;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;

class LembretesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $entradas = Lembrete::all();

      return view('pages.lembretes.index', compact('entradas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return view('pages.lembretes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $rules = array(
        'lembrete' => 'required|string',
      );

      $validator = validator::make(Input::all(), $rules);

      if ($validator->fails()) {
        return Redirect::to('/lembretes')
        ->withErrors($validator);
        // dd($validator);
      } else {
        // store
        $entrada = new Lembrete;
        $entrada->lembrete = Input::get('lembrete');
        $entrada->save();

        // redirect
        Session::flash('message', 'Sucesso!');
        return Redirect::to('/lembretes');
      }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Lembrete  $lembrete
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $entrada = Lembrete::find($id);

      return view('pages.lembretes.edit', compact('entrada'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Lembrete  $lembrete
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $rules = array(
        'lembrete' => 'required|string',
      );

      $validator = validator::make(Input::all(), $rules);

      if ($validator->fails()) {
        return Redirect::to('/lembretes')
        ->withErrors($validator);
        // dd($validator);
      } else {
        // store
        $entrada = Lembrete::find($id);
        $entrada->lembrete = Input::get('lembrete');
        $entrada->save();

        // redirect
        Session::flash('message', 'Sucesso!');
        return Redirect::to('/lembretes');
      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Lembrete  $lembrete
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      // delete
      $del = Lembrete::find($id);
      $del->delete();

      // redirect
      Session::flash('message', 'Removido com sucesso!');
      return Redirect::to('/lembretes');
    }
}
