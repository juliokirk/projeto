<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
{{-- <link rel="shortcut icon" href="{{{ asset('images/favicon.ico') }}}"> --}}
<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">

<title>
  @yield('title')
</title>

<!-- Styles -->
@stack('styles-before')
<link href="{{ asset('css/app.css') }}" rel="stylesheet">
@stack('styles-after')

<!-- Scripts -->
<script>
    window.Laravel = {!! json_encode([
        'csrfToken' => csrf_token(),
    ]) !!};
</script>
<script defer src="https://use.fontawesome.com/releases/v5.0.0/js/all.js"></script>
