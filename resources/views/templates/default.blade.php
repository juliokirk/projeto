<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
  <head>
    @include('partials.header')
  </head>
  <body>

    <!-- show page content -->
    @yield('content')

    <!-- Scripts -->
    @stack('scripts-before')
    <script src="{{ asset('js/app.js') }}"></script>
    @stack('scripts-after')

  </body>
</html>
