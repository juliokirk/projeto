@extends('templates.default')
@section('title')
  Lembrete | Editar | {{$entrada->id}}
@endsection

@section('content')

  <div class="container">

    <div class="row">
      <div class="col-md-6 col-sm-12">
        <form action="{{ route('lembretes.update', $entrada->id) }}" method="post">
          @method('PUT')
          @csrf

          <div class="form-group">
            <label for="exampleFormControlTextarea1">Editar lembrete</label>
            <textarea class="form-control rounded-0" id="lembrete" name="lembrete" rows="10">{{ $entrada->lembrete }}</textarea>
          </div>

          <input class="btn btn-primary" type="submit" value="Enviar">
          <input class="btn btn-warning" type="reset" value="Resetar">

        </form>

      </div>
    </div>
  </div>


@endsection
