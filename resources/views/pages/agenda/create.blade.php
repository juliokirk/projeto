@extends('templates.default')
@section('title')
  Agenda | Nova Entrada
@endsection

@section('content')

  <div class="container">

    <div class="row">
      <div class="col-md-6 col-sm-12">
        <form action="{{ route('agenda.store') }}" method="post">
          {{ csrf_field() }}

          <div class="form-group">
            <label for="exampleFormControlInput1">Nome</label>
            <input type="text" class="form-control" id="nome" name="nome">
          </div>
          <div class="form-group">
            <label for="exampleFormControlInput2">Endereço</label>
            <input type="text" class="form-control" id="endereco" name="endereco">
          </div>
          <div class="form-group">
            <label for="exampleFormControlInput3">E-mail</label>
            <input type="email" class="form-control" id="email" name="email">
          </div>
          <div class="form-group">
            <label for="exampleFormControlInput4">Telefone</label>
            <input type="number" class="form-control" id="telefone" name="telefone">
          </div>

          <input class="btn btn-primary" type="submit" value="Submit">
          <input class="btn btn-warning" type="reset" value="Reset">

        </form>

      </div>
    </div>
  </div>


@endsection
