@extends('templates.default')
@section('title')
  Lista
@endsection

@section('content')

  <table border="2px black">
    <thead>
      <tr>
        <td>ID</td>
        <td>Nome</td>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>1</td>
        <td>{{$data[0]}}</td>
      </tr>
      <tr>
        <td>2</td>
        <td>{{$data[1]}}</td>
      </tr>
      <tr>
        <td>3</td>
        <td>{{$data[2]}}</td>
      </tr>
      <tr>
        <td>4</td>
        <td>{{$data[3]}}</td>
      </tr>
    </tbody>
  </table>

@endsection
