<?php

use Illuminate\Database\Seeder;

class AgendasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('agendas')->insert([
                 'nome' => 'Jane Doe',
                 'endereco' => 'Rua 1, 221',
                 'email' => 'jane@mail.com',
                 'telefone' => '5555-5555',
             ]);

      DB::table('agendas')->insert([
                 'nome' => 'John Doe',
                 'endereco' => 'Rua 2, 331',
                 'email' => 'john@mail.com',
                 'telefone' => '5555-5555',
             ]);

    }
}
